from __future__ import unicode_literals

from django.db import models

class Cervecera(models.Model):
	nombre = models.CharField(max_length=200)
	website = models.URLField()
	pais = models.CharField(max_length=100)
	
	def __str__(self):
		return self.nombre


class Tipo(models.Model):
	nombre = models.CharField(max_length=100)
	descripcion = models.TextField()

	def __str__(self):
		return self.nombre


class Cerveza(models.Model):
	nombre = models.CharField(max_length=150)
	ibus = models.IntegerField()
	alcohol = models.FloatField()
	anadido = models.DateTimeField()
	valoracion = models.IntegerField()
	foto = models.ImageField(upload_to='beer-images/', null=True, blank=True)
	cervecera = models.ForeignKey(Cervecera, blank=True, on_delete=models.CASCADE)
	tipo = models.ForeignKey(Tipo, blank=True, on_delete=models.CASCADE)
	
	def __str__(self):
		return self.nombre	
