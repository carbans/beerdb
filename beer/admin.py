from django.contrib import admin
from models import Cerveza, Cervecera, Tipo


admin.site.register(Cerveza)
admin.site.register(Cervecera)
admin.site.register(Tipo)
